package com.vit.tam.perfcheck

class PerfCheck(numIterations: Int)(op: => Unit) {
  def run(): Long = {
    val startTime = System.currentTimeMillis()
    (0 until numIterations).foreach(i => op)
    System.currentTimeMillis() - startTime
  }
}
