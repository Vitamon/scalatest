package com.vit.tam

import com.vit.tam.perfcheck.PerfCheck
import com.vit.tam.splitter.{RegexpSplitter, STMSplitter, URI}


object Startup extends App {

  val numIterations = 10000

  val rexSplitter = new RegexpSplitter
  val stmSplitter = new STMSplitter

  val input = readLine()

  val stmResult = new PerfCheck(numIterations) ({
    URI(input)(stmSplitter)
  }).run()

  val rexResult = new PerfCheck(numIterations) ({
    URI(input)(rexSplitter)
  }).run()

  val url = URI(input)(rexSplitter)

  Seq(Option(url.scheme), Option(url.host), url.port, Option(url.path), url.parameters).
    flatten.foreach( println)

  println(s"Regex: ${rexResult}msec")
  println(s"STM: ${stmResult}msec")

}
