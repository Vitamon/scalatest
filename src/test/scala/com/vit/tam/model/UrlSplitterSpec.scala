package com.vit.tam.model

import com.vit.tam.splitter.{RegexpSplitter, STMSplitter, URI}
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class UrlSplitterSpec extends Specification {

  Seq(new RegexpSplitter, new STMSplitter).foreach { splitter =>

    s"${splitter.getClass}" should {

      "work for main case" in {
        URI("scheme://domain:8020/path/segment?query=ssdsd&fdfg=dfgdfg")(splitter) === URI("scheme", "domain", Some(8020), "/path/segment", Some("query=ssdsd&fdfg=dfgdfg"))
      }

      "work with no port and query" in {
        URI("scheme://domain/path")(splitter) === URI("scheme", "domain", None, "/path", None)
        URI("scheme:/domain/path")(splitter) === URI("scheme", "", None, "/domain/path", None)
        URI("scheme://domain:/path?")(splitter) === URI("scheme", "domain", None, "/path", None)
      }

      "throw if the url is malformed" in {
        URI("http::/")(splitter) should throwA[IllegalArgumentException]
      }
    }
  }
}
