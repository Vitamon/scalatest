package com.vit.tam.splitter

import org.apache.commons.lang.StringUtils

import scala.util.Try
import StringUtils.trimToNull

trait UrlSplitter {
  def split(url: String): URI
}

case class URI(scheme: String, host: String, port: Option[Int], path: String, parameters: Option[String])

object URI {
  def apply(url: String)(implicit splitter: UrlSplitter): URI = {
    splitter.split(url)
  }
}

object RegexpSplitter {
  // borrowed here:
  // http://www.leghumped.com/2008/11/03/java-matching-urls-with-regex-wildcards/
  val pattern = "^(?:(?![^:@]+:[^:@/]*@)([^:/?#.]+):)?(?://)?((?:(([^:@]*):?([^:@]*))?@)?([^:/?#]*)(?::(\\d*))?)(((/(?:[^?#](?![^?#/]*\\.[^?#/.]+(?:[?#]|$)))*/?)?([^?#/]*))(?:\\?([^#]*))?(?:#(.*))?)".r

  val parts = Map("source" -> 0, "protocol" -> 1, "authority" -> 2, "userInfo" -> 3, "user" -> 4, "password" -> 5,
    "host" -> 6, "port" -> 7, "relative" -> 8, "path" -> 9, "directory" -> 10, "file" -> 11, "query" -> 12, "ref" -> 13)
}

class RegexpSplitter extends UrlSplitter {

  import com.vit.tam.splitter.RegexpSplitter._

  def split(url: String): URI = {
    pattern.findFirstMatchIn(url).map(m => {
      URI(
        scheme = Option(trimToNull(m.group(parts("protocol")))).get,
        host = m.group(parts("host")),
        port = Try(m.group(parts("port")).toInt).toOption,
        path = m.group(parts("path")),
        parameters = Try(Option(trimToNull(m.group(parts("query"))))).getOrElse(None)
      )
    }).getOrElse(throw new IllegalArgumentException)
  }
}

class STMSplitter extends UrlSplitter {

  case class State(input: String, results: Map[String, String], nextState: Option[StateTransformer])

  type StateTransformer = State => State

  val Schema: String = "schema"
  val Host: String = "host"
  val Port: String = "port"
  val Path: String = "path"
  val Query: String = "query"

  def parseSchemaState: StateTransformer = { state =>
    val schema = state.input.takeWhile(_ != ':')
    val remaining = state.input.drop(schema.size + 1)

    if (remaining.startsWith("//")) {
      State(remaining.drop(2), state.results + (Schema -> schema), Some(parseHostState))
    } else if (remaining.startsWith("/")) {
      State(remaining, state.results + (Schema -> schema), Some(parsePathState))
    } else {
      throw new IllegalArgumentException("missing host or path")
    }
  }

  def parseHostState: StateTransformer = { state =>
    val host = state.input.takeWhile(ch => ch != '/' && ch != ':')
    val remaining = state.input.drop(host.size)
    State(remaining, state.results + (Host -> host), if (remaining.startsWith(":")) Some(parsePortState) else Some(parsePathState))
  }

  def parsePortState: StateTransformer = { state =>
    val port = state.input.drop(1).takeWhile(p => p >= '0' && p <= '9')
    State(state.input.drop(port.size + 1), state.results + (Port -> port), Some(parsePathState))
  }

  def parsePathState: StateTransformer = { state =>
    val path = state.input.takeWhile(_ != '?')
    val remaining = state.input.drop(path.size + 1)
    State(remaining, state.results + (Path -> path), if (remaining.size > 0) Some(parseQueryState) else None)
  }

  def parseQueryState: StateTransformer = { state =>
    State("", state.results + (Query -> state.input), None)
  }

  def stateProcessor(state: State): Map[String, String] = {
    state.nextState.fold(state.results)(next => stateProcessor(next(state)))
  }

  def split(url: String): URI = {
    val results = stateProcessor(new State(url, Map.empty, Some(parseSchemaState)))

    URI(
      scheme = results(Schema),
      host = results.get(Host).getOrElse(""),
      port = results.get(Port).flatMap(str => Option(StringUtils.trimToNull(str))).map(_.toInt),
      path = results(Path),
      parameters = results.get(Query)
    )
  }


}

